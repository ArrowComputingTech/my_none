#!/home/hz/.rbenv/shims/ruby

require './eech.rb'

module Enumerable
  def my_none?
    retr = []
    self.each do |x|
      if yield(x)
        retr.push(self[x])
      else
      end
    end
    return retr.length == 0 ? true : false
  end
end

arr = [5,4]
puts arr.my_none? { |x| x >= 3 }
puts arr.my_none? { |x| x <= 3 }
